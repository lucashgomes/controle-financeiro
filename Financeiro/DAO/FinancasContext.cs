﻿using Financeiro.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Financeiro.DAO
{
    public class FinancasContext : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<Movimentacao> Movimentacoes { get; set; }

        //Necessário sobrescrever o método devido a chave estrangeira Usuario no Movimentação
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Toda Movimentação tem um Usuario
            modelBuilder.Entity<Movimentacao>().HasRequired(movimentacao => movimentacao.Usuario);
        }
    }
}
