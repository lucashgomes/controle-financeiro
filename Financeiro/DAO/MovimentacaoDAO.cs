﻿using Financeiro.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Financeiro.DAO
{
    public class MovimentacaoDAO
    {
        private FinancasContext financasContext;

        public MovimentacaoDAO(FinancasContext financasContext)
        {
            this.financasContext = financasContext;
        }
        public void Adiciona(Movimentacao movimentacao)
        {
            financasContext.Movimentacoes.Add(movimentacao);
            financasContext.SaveChanges();
        }

        public IList<Movimentacao> Lista()
        {
            return financasContext.Movimentacoes.ToList();
        }

        public IList<Movimentacao> BuscaByUsuario(int? usuarioId)
        {
            return financasContext.Movimentacoes.Where(m => m.UsuarioId == usuarioId).ToList();
        }

        public IList<Movimentacao> Busca(decimal? valorMinimo, decimal? valorMaximo, DateTime? dataMinima, DateTime? dataMaxima, Tipo? tipo, int? usuarioId)
        {
            IQueryable<Movimentacao> busca = financasContext.Movimentacoes;
            if (valorMinimo.HasValue)
            {
                busca = busca.Where(m => m.Valor >= valorMinimo);
            }
            if (valorMaximo.HasValue)
            {
                busca = busca.Where(m => m.Valor <= valorMaximo);
            }

            if (dataMinima.HasValue)
            {
                busca = busca.Where(m => m.Data >= dataMinima);
            }
            if (dataMaxima.HasValue)
            {
                busca = busca.Where(m => m.Data <= dataMaxima);
            }

            if (tipo.HasValue)
            {
                busca = busca.Where(m => m.Tipo == tipo);
            }

            if (usuarioId.HasValue)
            {
                busca = busca.Where(m => m.UsuarioId == usuarioId);
            }

            return busca.ToList();
        }
    }
}