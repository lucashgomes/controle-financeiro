﻿using Financeiro.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Financeiro.DAO
{
    public class UsuarioDAO
    {
        private FinancasContext financasContext;

        public UsuarioDAO(FinancasContext financasContext)
        {
            this.financasContext = financasContext;
        }
        public void Adiciona(Usuario usuario)
        {
            financasContext.Usuarios.Add(usuario);
            financasContext.SaveChanges();
        }

        public IList<Usuario> Lista()
        {
            return financasContext.Usuarios.ToList();
        }
    }
}