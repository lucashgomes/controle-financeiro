﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Financeiro.Entidades
{
    public class Usuario
    {
        public int Id{ get; set; }

        [Required] 
        public string Nome{ get; set; }

        [Required, EmailAddress] //anotation para requerer campo e verificar se o email é válido
        public string Email{ get; set; }
    }
}
